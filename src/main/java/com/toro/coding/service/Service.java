package com.toro.coding.service;

import com.toro.coding.model.Movie;
import com.toro.coding.parse.ParseCsv;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import java.io.File;

public class Service {

    List<Movie> movies = new ArrayList<>();
    ParseCsv parsecs = new ParseCsv();
    Movie movie = new Movie();

    String fileName = "src/file/movies.csv";
    File file = new File(fileName);

    public List<Movie> getMovies() {
        movies = parsecs.parseCsv(file);
        return movies;
    }

    public Movie getMovieByTitle(String title) {
        movies = parsecs.parseCsv(file);
        Movie movie = new Movie();
        for (Movie mov : movies) {
            if (mov.getTitle().toLowerCase().equals(title.toLowerCase())) {
                movie = mov;
            }
        }
        return movie;
    }

    public Movie getMovieById(long id) {
        movies = parsecs.parseCsv(file);
        Movie movie = new Movie();
        for (Movie mov : movies) {
            if (mov.getId() == id) {
                movie = mov;
            }
        }
        return movie;
    }

    public List<Movie> getMovieByLanguage(String language) {
        movies = parsecs.parseCsv(file);
        List<Movie> moviesList = new ArrayList<>();

        for (Movie mov : movies) {
            if (mov.getLanguage().toLowerCase().equals(language.toLowerCase())) {
                moviesList.add(mov);
            }
        }
        return moviesList;
    }

    public List<Movie> getMovieByReleaseDate(String releaseDate) throws ParseException {
        movies = parsecs.parseCsv(file);
        List<Movie> moviesList = new ArrayList<>();
        Date date1 = new SimpleDateFormat("yyyy-MM-dd").parse(releaseDate);
        SimpleDateFormat sm = new SimpleDateFormat("MMMMM d, YYYY");
        String releaseDateFinal = sm.format(date1);
        for (Movie mov : movies) {
            if (mov.getReleaseDate().equals(releaseDateFinal)) {
                moviesList.add(mov);
            }
        }
        return moviesList;
    }

}
