package com.toro.coding.parse;

import com.toro.coding.model.Movie;

import java.io.File;
import java.io.FileNotFoundException;
import java.text.ParseException;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import java.util.Scanner;
import java.text.SimpleDateFormat;

public class ParseCsv {

    public ParseCsv() {
        super();
    }

    public List<Movie> parseCsv(File file) {

        Scanner inputStream;

        List<Movie> movies = new ArrayList<>();

        try {
            inputStream = new Scanner(file);
            while (inputStream.hasNext()) {
                    String line = inputStream.nextLine();
                    String[] values = line.split(",(?=(?:[^\"]*\"[^\"]*\")*[^\"]*$)", -1);
                    long id = 0;
                    boolean numeric = true;
                    try {
                       id  = Long.parseLong(values[0]);
                    } catch(NumberFormatException e) {
                        numeric = false;
                    }
                    if (numeric == true) {
                        String title = values[1];
                        String language = values[2];
                        String sDate1 = values[3];
                        Date date1 = new SimpleDateFormat("yyyy-MM-dd").parse(sDate1);
                        SimpleDateFormat sm = new SimpleDateFormat("MMMMM d, YYYY");
                        String releaseDate = sm.format(date1);
                        String runtime = values[4] + " min";
                        Movie movie = new Movie(id, title, language, releaseDate, runtime);
                        movies.add(movie);
                    }
            }

            inputStream.close();
        } catch (FileNotFoundException | ParseException e) {
            e.printStackTrace();
        }

        return movies;
    }

}
