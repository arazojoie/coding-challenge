package com.toro.coding.model;

import java.text.ParseException;

public class Movie {

    private long id;
    private String title;
    private String language;
    private String releaseDate;
    private String runtime;

    public Movie(long id, String title, String language, String releaseDate, String runtime) throws ParseException {
        this.id = id;
        this.title = title;
        this.language = language;
        this.releaseDate = releaseDate;
        this.runtime = runtime;
    }

    public Movie() {
        super();
    }

    public long getId() {
        return id;
    }

    public void setId(long id) {
        this.id = id;
    }

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public String getLanguage() {
        return language;
    }

    public void setLanguage(String language) {
        this.language = language;
    }

    public String getReleaseDate() {
        return releaseDate;
    }

    public void setReleaseDate(String releaseDate) {
        this.releaseDate = releaseDate;
    }

    public String getRuntime() {
        return runtime;
    }

    public void setRuntime(String runtime) {
        this.runtime = runtime;
    }



}
