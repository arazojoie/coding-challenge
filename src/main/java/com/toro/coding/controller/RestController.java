package com.toro.coding.controller;

import com.toro.coding.exception.ResourceNotFoundException;
import com.toro.coding.model.Movie;
import com.toro.coding.service.Service;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestParam;

import java.text.ParseException;
import java.util.List;

@org.springframework.web.bind.annotation.RestController
public class RestController {

    @GetMapping("/movies")
    public List<Movie> movie() {
        return new Service().getMovies();
    }

    @GetMapping("/movies/getByTitle")
    public Movie getByTitle(@RequestParam(value = "title") String title) {
        Movie movie = new Service().getMovieByTitle(title);
        if (movie.getId() == 0) {
            throw new ResourceNotFoundException("Movie with title " + title + " doesn't exist.");
        } else {
            return movie;
        }
    }

    @GetMapping("/movies/getByLanguage")
    public List<Movie> getByLanguage(@RequestParam(value = "language") String language) {
        List<Movie> movies = new Service().getMovieByLanguage(language);
        if (movies.size() == 0) {
            throw new ResourceNotFoundException("No movies with the language " + language );
        } else {
            return movies;
        }
    }

    @GetMapping("/movies/getByReleaseDate")
    public List<Movie> getByReleaseDate(@RequestParam(value = "releaseDate") String releaseDate) throws ParseException {
        List<Movie> movies = new Service().getMovieByReleaseDate(releaseDate);

        if (movies.size() == 0) {
            throw new ResourceNotFoundException("No movies with the release date " + releaseDate);
        } else {
            return movies;
        }
    }

    @GetMapping("/movies/getById")
    public Movie getById(@RequestParam(value = "id") long id) {
        Movie movie = new Service().getMovieById(id);
        if (movie.getId() == 0) {
            throw new ResourceNotFoundException("Movie with id " + id + " doesn't exist.");
        } else {
            return movie;
        }
    }
}
